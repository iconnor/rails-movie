rails-movie
===========

Sample trivial Rails app for tracking dvd/avi collection

Added Tire/ElasticSearch
 * http://railscasts.com/episodes/306-elasticsearch-part-1

To get started:
 * rake db:migrate
 * rake imdb:populate dirname="/Volumes/Data-1/Movies"

Thanks to http://imdbapi.org/ for their excellent API!